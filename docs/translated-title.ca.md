# Títol traduït

Aquesta pagina te un titol configurat a nav i està traduït a configuració del plugin i18n.

Veure <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/mkdocs.yml#L25> i <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/mkdocs.yml#L52>
