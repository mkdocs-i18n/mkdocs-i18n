# Índex en un subdirectori

Aquest document és la traducció del index.md en un subdirectori. Pàgina no traduïda al castellà.

Veure <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/mkdocs.yml#L49> i <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/docs/dir/index.ca.md>
